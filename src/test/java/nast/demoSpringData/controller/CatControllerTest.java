package nast.demoSpringData.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import nast.demoSpringData.service.CatService;

/**
 * Testing the integration between controller and the HTTP layer. 
 * @MockBean mocks away the business logic.
 */
@WebMvcTest(controllers = CatController.class)
class CatControllerTest {

	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CatService catService;

	
	@DisplayName("Get a cat by id - response status code should be 200")
	@Test
	void getCatById_ResponseStatusCodeOk() throws Exception {
		mockMvc.perform(get("/cats/900"))
			.andDo(print())
			.andExpect(status().isOk());
	}

}
