DROP TABLE 	IF EXISTS employees;

CREATE TABLE employees (
	id int NOT NULL primary key auto_increment,
	first_name varchar(255) NOT NULL,
	last_name varchar(255) NOT NULL,
	address varchar(255) NOT NULL,
	joining_date timestamp NOT NULL,
	updated_date timestamp ON UPDATE CURRENT_TIMESTAMP,
	deleted_date timestamp default NULL
);

DROP TABLE 	IF EXISTS cats;

CREATE TABLE cats (
  cat_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  colour varchar(255) NOT NULL
);

CREATE TABLE tags (
  tag_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(50) UNIQUE NOT NULL
)

CREATE TABLE  cat_class (
  cat_id INT NOT NULL,
  tag_id INT NOT NULL,
  PRIMARY KEY (cat_id,class_id),
  CONSTRAINT fk_tag_id FOREIGN KEY (tag_id) REFERENCES classes (tag_id),
  CONSTRAINT fk_cat_id FOREIGN KEY (cat_id) REFERENCES cats (cat_id)
);