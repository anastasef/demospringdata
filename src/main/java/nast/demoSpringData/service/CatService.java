package nast.demoSpringData.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import nast.demoSpringData.model.Cat;
import nast.demoSpringData.repositories.CatRepository;

@Service
public class CatService {

	@Autowired
	private CatRepository catRepository;

	public List<Cat> getAllCats() {
		return (List<Cat>) catRepository.findAll();
	}

	public String addCat(Cat cat) {
		String resp;

		if (catRepository.save(cat) != null)
			resp = "Successfully added!";
		else
			resp = "Something went wrong, cat not added.";

		return resp;
	}
	
	public Cat getCat(int id) {
		
		return catRepository.findById(id)
				.orElseThrow(() -> 
					new EntityNotFoundException("A cat with id " + id + " was not found."));
				
	}

	public String updateCat(Cat cat) {

		String resp;

		if (catRepository.save(cat) != null)
			resp = "Successfully updated!";
		else
			resp = "Something went wrong, cat not updated.";

		return resp;
	}

	public String deleteCat(int id) {

		String resp;

		try {
			catRepository.deleteById(id);
			resp = "Successfully updated!";
		} catch (Exception e) {
			resp = "Something went wrong, cat not updated.";
		}

		return resp;
	}

	public Map<String, Object> getCatsPage(int pageNum, int pageSize, String sortBy) {

		Pageable pageable = PageRequest.of(pageNum, pageSize, Sort.by(sortBy));
		Page<Cat> page = catRepository.findAll(pageable);

		return createPagedResponse(page);
	}

	public Map<String, Object> searchCatsByColour(int pageNum, int pageSize, String colour) {

		Pageable pageable = PageRequest.of(pageNum, pageSize);
		Page<Cat> page = catRepository.findByColour(colour, pageable);

		return createPagedResponse(page);
	}
	
	public Map<String, Object> searchCatsByNameAndColour(int pageNum, int pageSize, String name, String colour) {
		
		Pageable pageable = PageRequest.of(pageNum, pageSize);
		Page<Cat> page = catRepository.findByNameAndColour(name, colour, pageable);
		
		return createPagedResponse(page);
	}
	
	public Map<String, Object> searchCats(int pageNum, int pageSize, String searchWord) {
		
		Pageable pageable = PageRequest.of(pageNum, pageSize);
		Page<Cat> page = catRepository.findByColourOrName(searchWord, searchWord, pageable);
		
		return createPagedResponse(page);
		
	}
	
	public Map<String, Object> searchCatsIgnoreCase(int pageNum, int pageSize, String searchWord) {

		Pageable pageable = PageRequest.of(pageNum, pageSize);
		Page<Cat> page = catRepository.findCatsIgnoreCase(searchWord, pageable);

		return createPagedResponse(page);
	}
	
	public Map<String, Object> searchCatsByNameUsingNamedQuery(int pageNum, int pageSize, String name) {

		Pageable pageable = PageRequest.of(pageNum, pageSize);
		Page<Cat> page = catRepository.customFindByNameUsingNamedQuery(name, pageable);

		return createPagedResponse(page);
	}

	/**
	 * Standard paged response.<br>
	 * Keys of the map will be the keys in the json response.
	 * @param page Response from the repository method.
	 * @return Returns a map of page's properties and the found data.
	 */
	private Map<String, Object> createPagedResponse(Page<Cat> page) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		resp.put("count", page.getTotalElements());
		resp.put("current-page", page.getNumber());
		resp.put("total-pages", page.getTotalPages());
		resp.put("data", page.getContent());

		return resp;
	}



	

}
