package nast.demoSpringData.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nast.demoSpringData.model.Employee;
import nast.demoSpringData.repositories.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	public Optional<Employee> getEmployeeById(int id) {
		
		Employee res = employeeRepository.findEmployeeById(id);
		
		return (res == null) ? Optional.empty() : Optional.of(res);
	}

	
	public List<Employee> getAll() {
		return employeeRepository.findAll();
	}

	public String addEmployee(Employee emp) {
		String resp;

		if (employeeRepository.addEmployee(emp))
			resp = "Successfully added.";
		else
			resp = "Something went wrong, not added.";

		return resp;
	}

	public String updateEmployee(Employee emp) {

		String resp;

		if (employeeRepository.updateEmployee(emp))
			resp = "Successfully updated.";
		else
			resp = "Something went wrong, not updated.";

		return resp;
	}

	public String deleteEmployee(int id) {
		String resp;

		if (employeeRepository.deleteEmployee(id))
			resp = "Successfully deleted.";
		else
			resp = "Something went wrong, not deleted.";

		return resp;
	}

}
