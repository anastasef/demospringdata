package nast.demoSpringData.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Entity
@NamedQueries({
		@NamedQuery(name = "Cat.customFindByNameUsingNamedQuery", 
				query = "SELECT c FROM Cat c WHERE LOWER(c.name) LIKE LOWER(concat('%', :search, '%'))") })
@Table(name = "cats")
public class Cat implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cat_id", unique = true, nullable = false)
	private int id;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String colour;
	
	public Cat(String name, String colour) {
		this.name = name;
		this.colour = colour;
	}
	
	public Cat(String name, String colour, Set<Tag> tags) {
		this.name = name;
		this.colour = colour;
		if (!tags.isEmpty()) this.catTags = tags;
	}

	// FetchType.LAZY is a hint to the persistence provider runtime that 
	// data should be fetched when it is first accessed
	// This class is responsible of handling the “connection” between 
	// classes of Cat and Class
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "cat_tag", 
		joinColumns = {
				@JoinColumn(name = "cat_id", nullable = false, updatable = false)
		}, 
		inverseJoinColumns = {
				@JoinColumn(name = "tag_id", nullable = false, updatable = false) 
		})
	private Set<Tag> catTags = new HashSet<Tag>(0);
}
