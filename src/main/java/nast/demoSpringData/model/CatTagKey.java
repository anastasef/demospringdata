package nast.demoSpringData.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CatTagKey implements Serializable {
	
	private static final long serialVersionUID = -1419565596724384867L;
	
	@Column(name = "cat_id")
	private Integer catId;
	
	@Column(name = "tag_id")
	private Integer tagId;
	
//	@Override
//	public int hashCode() {
//		final int PRIME = 42;
//		int result = 1;
//		result = result * PRIME + ((catId == null) ? 0 : catId.hashCode());
//		result = result * PRIME + ((tagId == null) ? 0 : tagId.hashCode());
//		
//		return result;
//	}
//	
//	@Override
//	public boolean equals(Object o) {
//		
//		if (this == o) return true;
//		if (o == null) return false;
//		if (getClass() != o.getClass()) return false;
//		CatTagKey other = (CatTagKey) o;
//		if (catId == null) {
//			if (other.catId != null) return false;
//		} else if (!catId.equals(other.catId)) return false;
//	}

}
