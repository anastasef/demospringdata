package nast.demoSpringData.model;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Employee {
	
	private int id;
	private String firstName;
	private String lastName;
	private String address;
	private LocalDateTime joiningDate;
	private LocalDateTime updatedDate;
	private LocalDateTime deletedDate;
	
	
}
