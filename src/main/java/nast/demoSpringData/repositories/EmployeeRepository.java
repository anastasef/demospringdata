package nast.demoSpringData.repositories;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import nast.demoSpringData.model.Employee;

@Repository
public class EmployeeRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	private final String GET_EMPLOYEE_BY_ID = "SELECT * FROM employees WHERE ID = ?";
	private final String GET_ALL = "SELECT * FROM employees WHERE deleted_date IS NULL";
	private final String INSERT_EMPLOYEE = "INSERT INTO employees (first_name, last_name, address, joining_date, updated_date) values (?, ?, ?, ?, ?)";
	private final String UPDATE_EMPLOYEE = "UPDATE employees SET first_name = ?, last_name = ?, address = ?  WHERE id = ?";
	private final String DELETE_EMPLOYEE = "UPDATE employees SET deleted_date = ? WHERE id = ?";

	private RowMapper<Employee> rowMapper = (ResultSet rs, int rowNum) -> {
		Employee emp = new Employee();

		emp.setId(rs.getInt(1));
		emp.setFirstName(rs.getString(2));
		emp.setLastName(rs.getString(3));
		emp.setAddress(rs.getString(4));
		emp.setJoiningDate(rs.getTimestamp(5).toLocalDateTime());
		
		Timestamp updatedDate = rs.getTimestamp(6);
		if (rs.wasNull()) {
			emp.setUpdatedDate(null);
		} else {
			emp.setUpdatedDate(updatedDate.toLocalDateTime());
		}
		
		Timestamp deletedDate = rs.getTimestamp(7);
		if (rs.wasNull()) {
			emp.setDeletedDate(null);
		} else {
			emp.setDeletedDate(deletedDate.toLocalDateTime());
		}

		return emp;
	};
	
	public Employee findEmployeeById(int id) {
		// Instead of returning a null when record not found,
		// Spring throws an EmptyResultDataAccessException.
		try {
			return jdbcTemplate.queryForObject(GET_EMPLOYEE_BY_ID, new Object[]{id}, rowMapper);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
		
	}

	public List<Employee> findAll() {
		return jdbcTemplate.query(GET_ALL, rowMapper);
	}

	public boolean addEmployee(Employee emp) {
		if (jdbcTemplate.update(INSERT_EMPLOYEE, 
				emp.getFirstName(), emp.getLastName(), emp.getAddress(), LocalDateTime.now(), null) > 0)
			return true;
		return false;
	}

	public boolean updateEmployee(Employee emp) {
		if (jdbcTemplate.update(UPDATE_EMPLOYEE, emp.getFirstName(), emp.getLastName(), emp.getAddress(),
				emp.getId()) > 0)
			return true;
		return false;
	}

	public boolean deleteEmployee(int id) {
		if (jdbcTemplate.update(DELETE_EMPLOYEE, LocalDateTime.now(), id) > 0)
			return true;
		return false;
	}

}
