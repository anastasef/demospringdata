package nast.demoSpringData.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import nast.demoSpringData.model.Cat;

@Repository
public interface CatRepository extends JpaRepository<Cat, Integer> {
	
	public static final String FIND_CATS_IGNORE_CASE_QUERY = 
			"SELECT c FROM Cat c " +
			"WHERE LOWER(c.name) LIKE LOWER(concat('%', :search, '%')) " +
			"OR LOWER(c.colour) LIKE LOWER(concat('%', :search, '%'))";
	
	/**
	 * Method for NamedQuery demonstration.
	 * @param name
	 * @param pageable
	 * @return
	 */
	public Page<Cat> customFindByNameUsingNamedQuery(@Param("search") String name, Pageable pageable);

	public Page<Cat> findByColour(String colour, Pageable pageable);

	public Page<Cat> findByColourOrName(String colour, String name, Pageable pageable);
	
	public Page<Cat> findByNameAndColour(String name, String colour, Pageable pageable);
	
	@Query(FIND_CATS_IGNORE_CASE_QUERY)
	public Page<Cat> findCatsIgnoreCase(@Param("search") String searchWord, Pageable pageable);
}
