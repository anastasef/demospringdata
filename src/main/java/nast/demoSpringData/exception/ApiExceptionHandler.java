package nast.demoSpringData.exception;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {

	@ExceptionHandler(value = {ApiRequestException.class})
	public ResponseEntity<Object> handleApiRequestException(ApiRequestException e) {
		
		// 1. Create payload containing exception details

		HttpStatus badRequest = HttpStatus.BAD_REQUEST;

		ApiException apiException = new ApiException(
				e.getMessage(),
				badRequest,
				ZonedDateTime.now(ZoneId.of("Europe/Moscow")));

		// 2. Return response entity
		
		return new ResponseEntity<Object>(apiException, badRequest);
	};
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<Object> handleResourceNotFoundException(ResourceNotFoundException e) {
		HttpStatus notFound = HttpStatus.NOT_FOUND;
		
		ApiException apiException = new ApiException(
				e.getMessage(), 
				notFound, 
				ZonedDateTime.now(ZoneId.of("Europe/Moscow")));
		return new ResponseEntity<Object>(apiException, notFound);
	}
}
