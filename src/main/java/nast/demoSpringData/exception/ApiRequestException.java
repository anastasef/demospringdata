package nast.demoSpringData.exception;

@SuppressWarnings("serial")
public class ApiRequestException extends RuntimeException {

	public ApiRequestException(String message) {
		super(message);
	}
	
}
