package nast.demoSpringData.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nast.demoSpringData.exception.ResourceNotFoundException;
import nast.demoSpringData.model.Employee;
import nast.demoSpringData.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getEmployeeById(@PathVariable("id") int id) {
		
		return employeeService.getEmployeeById(id)
				.map(empData -> ResponseEntity.ok(empData))
				.orElseThrow(() -> new ResourceNotFoundException("Employee with id " + id + "not found"));
	}
	
	@GetMapping("")
	public List<Employee> getAll() {
		return employeeService.getAll();
	}
	
	@PostMapping("")
	public String addEmployee(@RequestBody Employee emp) {
		return employeeService.addEmployee(emp);
	}
	
	@PutMapping("")
	public String updateEmployee(@RequestBody Employee emp) {
		return employeeService.updateEmployee(emp);
	}
	
	@DeleteMapping("/{id}")
	public String deleteEmployee(@PathVariable("id") int id) {
		return employeeService.deleteEmployee(id);
	}
}
