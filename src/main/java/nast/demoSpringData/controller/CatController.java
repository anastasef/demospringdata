package nast.demoSpringData.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nast.demoSpringData.model.Cat;
import nast.demoSpringData.service.CatService;

@RestController
@RequestMapping("/cats")
public class CatController {

	@Autowired
	private CatService catService;
	
	@GetMapping("")
	public List<Cat> getAllCats() {
		return catService.getAllCats();
	}
	
	@PostMapping("")
	public String addCat(@RequestBody Cat cat) {
		return catService.addCat(cat);
	}
	
	@GetMapping("/{id}")
	public Cat getCat(@PathVariable("id") int id) {
		return catService.getCat(id);
	}
	
//	@PutMapping("/{id}")
//	public String updateCat(@PathVariable("id") int id, @RequestBody Cat cat) {
//		cat.setId(id);
//		return catService.updateCat(cat);
//	}
	
	@DeleteMapping("/{id}")
	public String deleteCat(@PathVariable("id") int id) {
		return catService.deleteCat(id);
	}
	
	@GetMapping("/pages")
	public Map<String, Object> getCatsPage(
			@RequestParam(value = "page", required = false, defaultValue = "0") int pageNum,
			@RequestParam(value = "size", required = false, defaultValue = "30") int pageSize,
			@RequestParam(value = "sort", required = false, defaultValue = "id") String sortBy) {
		return catService.getCatsPage(pageNum, pageSize, sortBy);
	}
	
	@GetMapping("/search")
	public Map<String, Object> searchCatsByColour(
			@RequestParam(value = "page", required = false, defaultValue = "0") int pageNum,
			@RequestParam(value = "size", required = false, defaultValue = "3") int pageSize,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "colour", required = false) String colour) {
		
		if (!name.isEmpty()) {
			if (!colour.isEmpty()) {
				return catService.searchCatsByNameAndColour(pageNum, pageSize, name, colour);
			} else {
				return catService.searchCatsByNameUsingNamedQuery(pageNum, pageSize, name);
			}
		} else if (!colour.isEmpty()) {
			return catService.searchCatsByColour(pageNum, pageSize, colour);
		} else {
			return catService.getCatsPage(pageNum, pageSize, "name");
		}
		
	}
	
	@GetMapping("/search-name")
	public Map<String, Object> searchCatsByName(
			@RequestParam(value = "page", required = false, defaultValue = "0") int pageNum,
			@RequestParam(value = "size", required = false, defaultValue = "3") int pageSize,
			@RequestParam(value = "name") String name) {
		return catService.searchCatsByNameUsingNamedQuery(pageNum, pageSize, name);
	}
	
	@GetMapping("/global-search")
	public Map<String, Object> searchCats(
			@RequestParam(value = "page", required = false, defaultValue = "0") int pageNum,
			@RequestParam(value = "size", required = false, defaultValue = "3") int pageSize,
			@RequestParam(value = "search") String searchWord) {
		return catService.searchCats(pageNum, pageSize, searchWord);
	}
	
	@GetMapping("/global-ignore-case-search")
	public Map<String, Object> searchCatsIgnoreCase(
			@RequestParam(value = "page", required = false, defaultValue = "0") int pageNum,
			@RequestParam(value = "size", required = false, defaultValue = "3") int pageSize,
			@RequestParam(value = "search") String searchWord) {
		return catService.searchCatsIgnoreCase(pageNum, pageSize, searchWord);
	}
	
}
