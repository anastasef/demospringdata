# Demo Spring Data Project

This project was done in order to get my feet wet with Spring Data JPA.

*Tools used:*

- Spring Boot
- Spring Web
- Spring Data JPA
- H2 Database
- Lombok

## Links

### Spring Data

1. [Video tutorial on H2 database](https://www.youtube.com/watch?v=4-Mhrh3M0co)
2. [Spring Data JPA Query Methods](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation)

### Designing URIs

1. [API requests naming conventions](https://api.gov.au/standards/national_api_standards/naming-conventions.html)
2. [Article "Cool URIs don't change" by Tim Berners-Lee](https://www.w3.org/Provider/Style/URI)
3. [Best practices for RESTful web services : Naming conventions and API Versioning](https://hub.packtpub.com/best-practices-for-restful-web-services-naming-conventions-and-api-versioning-tutorial/)
4. [:heavy_exclamation_mark: REST Resource Naming Guide](https://restfulapi.net/resource-naming/)

